<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<link rel="stylesheet" href="style.css"/>
</head>
<body>

	<div class="container">
		<header>
			<h1>Follow Me On Social Media</h1>
		</header>

		<ul class="accordion">
			<li class="tab">
				<div class="social youtube"><a href="https:://www.youtube.com" target="_blank">Youtube</a>
				</div>
				<div class="content">
					<h1>Youtube</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis, dolorem!</p>
				</div>
			</li>
			<li class="tab">
				<div class="social instagram">
					
					<a href="https:://www.instagram.com" target="_blank">Instagram</a>
				</div>
				<div class="content">
					<h1>Instagram</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, aspernatur!</p>
				</div>
			</li>
			<li class="tab">
				<div class="social facebook"><a href="https:://www.facebook.com" target="_blank">Facebook</a>
				</div>
				<div class="content">
					<h1>Facebook</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, beatae.</p>
				</div>
			</li>
			<li class="tab">
				<div class="social twitter"><a href="https:://www.twitter.com" target="_blank">Twitter</a>
				</div>
				<div class="content">
					<h1>Twitter</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A cupiditate assumenda quod quam.</p>
				</div>
			</li>
			<li class="tab">
				<div class="social linkedin"><a href="https:://www.linkedin.com" target="_blank">LinkedIn</a>
				</div>
				<div class="content">
					<h1>LinkedIn</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae deleniti omnis libero illum vero, numquam.</p>
				</div>
			</li>
			<li class="tab">
				<div class="social github"><a href="https:://www.github.com" target="_blank">Github</a>
				</div>
				<div class="content">
					<h1>Github</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae accusantium iusto magni nulla illum ratione, corrupti.</p>
				</div>
			</li>
		</ul>
	</div>
	
</body>


</html>